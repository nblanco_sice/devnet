﻿using System;

namespace AsyncTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, launching random delay tasks asinchronously !");
            var t = new TaskManager();
            t.TaskLauncher();
            Console.WriteLine("All tasks have been launched.Main program ended");
            Console.ReadKey();

        }
    }
}
