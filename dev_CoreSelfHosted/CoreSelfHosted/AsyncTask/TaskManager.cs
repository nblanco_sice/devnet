﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncTask
{
    public class TaskManager
    {
        public Random random { get; set; }

        public TaskManager()
        {
            random = new Random(DateTime.Now.Millisecond);
        }

        public void TaskLauncher(int number=10)
        {
            for (int i = 1; i < number; i++)
            {
                var wait_for = getRandom();
                Console.WriteLine("Launching task " + i);
                LaunchTasksAsync(i, wait_for);
            }
        }

        public async void LaunchTasksAsync(int number,int wait_for)
        {
                Console.WriteLine( await RandomTask(number, wait_for));
        }

        private int getRandom()
        {
            
            int r = this.random.Next(1, 60);
            return r;
        }

        public async Task<string> RandomTask(int id, int wait_for)
        {

            Console.WriteLine("Hi, this is task " + id + ".I will wait for " + wait_for + " seconds");
            await Task.Delay(wait_for*1000);
            string msg="Hi, this is task " + id + ".I finished after " + wait_for + " seconds";
            return msg;
        }
    }
}
