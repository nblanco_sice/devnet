﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Topshelf;

namespace ConsoleApp1
{
    public class TopShelf : ServiceControl
    {
        private System.Timers.Timer _timer = new System.Timers.Timer();

        public bool Start(HostControl hostControl)
        {
            Console.WriteLine("The Service was Started!");

            _timer = new Timer(3000) { AutoReset = true };
            _timer.Elapsed += OnTimedEvent;
            _timer.Start();
            return true;
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            _timer.Enabled = false;

            Console.WriteLine("Hola, son las "+DateTime.Now.ToString("HH:mm:ss"));

            _timer.Enabled = true;
        }

        public bool Stop(HostControl hostControl)
        {
            Console.WriteLine("The Service was Stopped!");
            _timer.Stop();
            return true;
        }
    }
}

