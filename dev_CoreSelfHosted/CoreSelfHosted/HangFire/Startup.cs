﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace HangFire
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {

        }


        public void Configure(IApplicationBuilder app)
        {
            var mem_options = new MemoryStorageOptions();
            mem_options.JobExpirationCheckInterval = TimeSpan.FromSeconds(1);
            mem_options.FetchNextJobTimeout = TimeSpan.FromSeconds(1);
            mem_options.CountersAggregateInterval = TimeSpan.FromSeconds(1);
            GlobalConfiguration.Configuration.UseMemoryStorage(mem_options);
            app.UseHangfireDashboard();
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter() }
            });
        }
        public void ConfigureServices(IServiceCollection services)
        {
            var mem_options = new MemoryStorageOptions();
            mem_options.JobExpirationCheckInterval = TimeSpan.FromSeconds(1);
            mem_options.FetchNextJobTimeout = TimeSpan.FromSeconds(1);
            mem_options.CountersAggregateInterval = TimeSpan.FromSeconds(1);
            services.AddHangfire(config =>
                config.UseMemoryStorage(mem_options));
        }

        }
    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return httpContext.User.Identity.IsAuthenticated;
        }
    }
}
