﻿using Hangfire;
using Hangfire.Common;
using Hangfire.MemoryStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Topshelf;

namespace ConsoleApp1
{
    public class HangFireClass : ServiceControl
    {
        public bool Start(HostControl hostControl)
        {
            Console.WriteLine("The Service was Started!");

            var mem_options = new MemoryStorageOptions();
            mem_options.JobExpirationCheckInterval = TimeSpan.FromSeconds(1);
            mem_options.FetchNextJobTimeout = TimeSpan.FromSeconds(1);
            mem_options.CountersAggregateInterval=TimeSpan.FromSeconds(1);
            JobStorage.Current = new MemoryStorage(mem_options);


            var options = new RecurringJobOptions();
            options.QueueName = "test1";
            var manager = new RecurringJobManager();

            Console.WriteLine("Launching job at " + DateTime.Now.ToString("HH:mm:ss"));
            manager.AddOrUpdate("job1", Job.FromExpression(() => Console.WriteLine("Hola, son las " + DateTime.Now.ToString("HH:mm:ss"))), Cron.Minutely(), options);

            //RecurringJob.AddOrUpdate(() => Console.WriteLine("Hola, son las " + DateTime.Now.ToString("HH:mm:ss"), Cron.Minutely());

            var jobId = BackgroundJob.Schedule(() => Console.WriteLine("Hola, son las " + DateTime.Now.ToString("HH:mm:ss")), TimeSpan.FromSeconds(7));
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Console.WriteLine("The Service was Stopped!");
            
            return true;
        }

        public static void RandomJob1()
        {
            Console.WriteLine("Hola, son las " + DateTime.Now.ToString("HH:mm:ss"));
        }
    }
}

