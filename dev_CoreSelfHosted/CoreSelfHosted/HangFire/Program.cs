﻿using ConsoleApp1;
using System;
using Topshelf;

namespace HangFire
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(host =>
            {
                host.SetServiceName("SelfHostedNico1");
                host.SetDisplayName("SelfHostedNico1");
                host.SetDescription("SelfHostedNico1");
                host.StartAutomatically();

                host.Service<HangFireClass>();
            });
        }

        
    }
}
