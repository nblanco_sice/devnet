﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitPublisherNET
{
    class Program
    {
        static void Main(string[] args)
        {
            var publiser = new MqPublisher();
            //publiser.PublishMessagesSingle(60);
            publiser.PublishMessagesBatch(60);
            Console.ReadKey();

        }
    }
}
