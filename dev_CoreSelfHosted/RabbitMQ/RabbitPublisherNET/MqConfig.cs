﻿using System.Configuration;

namespace RabbitPublisherNET
{
    public class MqConfig
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string VirtualHost { get; set; }

        public string HostName { get; set; }

        public string Uri { get; set; }

        public string Queue { get; set; }

        public bool Durable { get; set; }


        public MqConfig()
        {
            //string keyvalue = Properties.Settings.Default.keyname;
            Username= ConfigurationManager.AppSettings["mq_username"];
            Password = ConfigurationManager.AppSettings["mq_password"];
            VirtualHost = ConfigurationManager.AppSettings["mq_virtualhost"]; 
            HostName = ConfigurationManager.AppSettings["mq_hostname"]; 
            Uri = ConfigurationManager.AppSettings["mq_uri"];
            Queue = ConfigurationManager.AppSettings["mq_queue"];
            Durable =bool.Parse(ConfigurationManager.AppSettings["mq_queue_durable"]);

        }

    }
}
