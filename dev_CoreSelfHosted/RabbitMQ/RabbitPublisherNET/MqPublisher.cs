﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitPublisherNET
{
    public class MqPublisher
    {
        private MqService _service { get; set; }
        private MqConfig _config { get; set; }

        public MqPublisher()
        {
            _config = new MqConfig();
            _service = new MqService(_config);
        }

        public void PublishMessagesSingle(int seconds)
        {
            var list = getRandomMessages(100);
            DateTime start_time = DateTime.Now;
            DateTime end_time = start_time.AddSeconds(seconds);
            int counter = 0;

            while (DateTime.Now < end_time)
            {
                foreach (var message in list)
                {
                    _service.PublishMessageSingle(message);
                    counter++;
                }
            }
            Console.WriteLine("Published " + counter + " messages in " + seconds + " seconds");
        }

        public void PublishMessagesBatch(int seconds)
        {
            var list = getRandomMessages(10000);
            DateTime start_time = DateTime.Now;
            DateTime end_time = start_time.AddSeconds(seconds);
            int counter = 0;

            while (DateTime.Now < end_time)
            {
               _service.PublishMessageBatch(list);
                counter = counter + list.Count();
            }
            Console.WriteLine("Published " + counter + " messages in " + seconds + " seconds");
        }


        private List<object> getRandomMessages(int num)
        {
            var list = new List<object>();
            for(var i=0;i<num;i++)
            {
                list.Add(Randomizer.RandomString(50));
            }
            return list;
        }



    }
}
