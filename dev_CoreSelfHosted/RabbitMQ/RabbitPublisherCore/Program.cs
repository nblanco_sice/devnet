﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace RabbitPublisherCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var publiser = new MqPublisher();
            //publiser.PublishMessagesSingle(60);
            publiser.PublishMessagesBatch(60);
            Console.ReadKey();
        }
    }


}
