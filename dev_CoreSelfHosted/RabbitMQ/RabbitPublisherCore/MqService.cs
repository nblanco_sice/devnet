﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitPublisherCore
{
    public class MqService
    {
        private readonly MqConfig options;
        private readonly ConnectionFactory connectionFactory;
        
        public MqService(MqConfig _options)
        {
            options = _options;
            connectionFactory = new ConnectionFactory
            {
                UserName = options.Username,
                Password = options.Password,
                VirtualHost = options.VirtualHost,
                HostName = options.HostName,
                Uri = new Uri(options.Uri)
            };
            Purge();
        }

        public void PublishMessageSingle(object message)
        {
            using (var conn = connectionFactory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: options.Queue,
                        durable: options.Durable,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );

                    var jsonPayload = JsonConvert.SerializeObject(message);
                    var body = Encoding.UTF8.GetBytes(jsonPayload);

                    channel.BasicPublish(exchange: "",
                        routingKey: options.Queue,
                        basicProperties: null,
                        body: body
                    );
                    
                }
            }
        }

        public void PublishMessageBatch(List<object> messages)
        {
            using (var conn = connectionFactory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: options.Queue,
                        durable: options.Durable,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );
                    foreach (var message in messages)
                    {
                        var jsonPayload = JsonConvert.SerializeObject(message);
                        var body = Encoding.UTF8.GetBytes(jsonPayload);

                        channel.BasicPublish(exchange: "",
                            routingKey: options.Queue,
                            basicProperties: null,
                            body: body
                        );
                    }
                }
            }
        }

        public void Purge()
        {
            using (var conn = connectionFactory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: options.Queue,
                        durable: options.Durable,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );
                    channel.QueuePurge(options.Queue);
                }
            }
        }
    }
}