﻿using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace RabbitPublisherCore
{
    public class MqConfig
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string VirtualHost { get; set; }

        public string HostName { get; set; }

        public string Uri { get; set; }

        public string Queue { get; set; }

        public bool Durable { get; set; }


        public MqConfig()
        {
            var builder = new ConfigurationBuilder()
            .AddJsonFile("appSettings.json");
            var Configuration = builder.Build();
            //string keyvalue = Properties.Settings.Default.keyname;
            Username = Configuration["rabbitmq:mq_username"];
            Password = Configuration["rabbitmq:mq_password"];
            VirtualHost = Configuration["rabbitmq:mq_virtualhost"]; 
            HostName = Configuration["rabbitmq:mq_hostname"]; 
            Uri = Configuration["rabbitmq:mq_uri"];
            Queue = Configuration["rabbitmq:mq_queue"];
            Durable =bool.Parse(Configuration["rabbitmq:mq_queue_durable"]);

        }

    }
}
